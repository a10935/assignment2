### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 19286654-d654-44d5-8b7f-73507b45889a
#activate the Pkg
Pkg.activate("problem2")

# ╔═╡ 8c1cee7e-4fff-4484-9c9e-708fc94bfc4a
#Tools that i might need

using PlutoUI, DataFrames, Plots, Lathe, GLM, Statistics, StatsPlots, MLBase

# ╔═╡ 83221e07-9284-4034-bd13-1b5fb2fce504
#More tools to use 
using Markdown,InteractiveUtils

# ╔═╡ c046cf40-cbe8-11ec-23f8-37cc023be132
md"Problem 2!"

# ╔═╡ 3bbc4486-6ef1-47c2-a9f5-df976ef6a11b
md"Constraint satisfaction problem model"

# ╔═╡ b0f78aa0-137f-4f73-bb0a-7d6a9e7f0dd9
#serverless function
#Forward checking and propagation

# ╔═╡ 914c1846-0ca3-4f26-9d34-6e836d091630


# ╔═╡ 2e8211c4-b663-49b4-bf58-a80ab3777698


# ╔═╡ 24083e91-f694-4c65-b71f-573d8c1ee002


# ╔═╡ cefa56d1-d2e4-4269-96a2-b98975319d5e
#Create an Enum{BaseType} subtype with name EnumName and enum member values of value1 and value2 with optional assigned values of x and y, respectively. EnumName can be used just like other types and enum member values as regular values.

@enum numSign one two three four


# ╔═╡ a2f19e69-fd5b-42cf-9a2d-e2dc48979bed
#mutable struct is similar to struct, but additionally allows the fields of the type to be set after construction. 


mutable struct Actions
	name::String
	value::Union{Nothing, numSign}
	not_permitted::Vector{numSign}
	constraint::Vector{min}
	action_restriction_count::Int64
end

# ╔═╡ 98b01c86-1ca6-4932-973b-8c1cd2772f0a
#a name and set of fields. 
#the constraints are declared here.

struct numcloud
	var::Vector{numSign}
	constraints::Vector{Tuple{numSign,numSign}}
	constraints_time::Vector{min,min}
	constaints_latency::Vector{max,max}
	constraints_throughput::Vector{max,max}
	constraints_success::Vector{min,min}
end

# ╔═╡ 739a2c22-2de3-4ac0-8ff9-35a615b8816f
#trying something 
function whichmin( x::Vector )
  i = 1
  min_x=minimum(x)
  while( x[i] > min_x ) 
    i+=1 
  end
  return i
end

# ╔═╡ 22df6018-4ac6-443a-bc36-49fb68901b19
function whichmax( y::Vector )
  i = 1
  max_y=maximum(y)
  while( x[i] < man_y ) 
    i+=1 
  end
  return i
end

# ╔═╡ ff8bd2d0-dafa-437b-aedf-651af3df3f50
#Function section, what I want the model to do.
function solve_cloud(pb::numcloud, all_assignments)
	for current_var in pb.var
		if current_var.action_restriction_count==4
			return []
		else
			next_val = rand(setdiff(Set([one,two,three,four]), Set(current_var.not_permitted)))
			
			for current_constraint in pb.constraints
				if !((current_constraint[1] == current_var) || (current_constraint[3] == current_var))
					continue
				else
					if current_constraint[1]==current_var
						push!(current_constraint[3].not_permitted, next_val)
						current_constraint[3].action_restriction_count +=1
					else
						push!(current_constraint[1].not_permitted, next_val)
						current_constraint[1].action_restriction_count +=1
					end
				end
			end
			push!(all_assignments, current_var.name=>next_val)
		end
	end
	return all_assignments
end


# ╔═╡ 77b77df9-dd60-450a-b4fa-f06c1b2f5a69


# ╔═╡ 8695804e-3fbb-4c6a-9a77-d0f80016985d
#add forword checking
function forward_check(pb::numcloud, current_var)
	for current_constraint in pb.constraints
		if !((current_constraint[1] == current_var) || (current_constraint[3] == current_var))
			pop!(current_constraint[3], current_var)
		end
	end
	if (length(pb.current_action_restriction==0))
		return false
	end
end

# ╔═╡ 1d414422-9b46-400d-be57-dc7fb5b81b51


# ╔═╡ 5f15513f-fcc3-43f5-beb4-d308ada577e2
1 = Actions("1",empty,[],0)

# ╔═╡ 7ccfd548-d31d-4432-a480-e3b93bfed7ea
2 = Actions("2",empty,[],0)

# ╔═╡ 4eeba374-7634-48dc-96a9-70fb2dc030bc
3 = Actions("3",empty,[four,three,two],0)

# ╔═╡ 0065f708-8326-47e9-baf1-66ecfc7d7dda
4 = Actions("4",empty,[],0)

# ╔═╡ 6992ebb4-5611-4944-8d77-e970a2455aa3


# ╔═╡ fb88765d-c402-4fab-92cd-ba793a97a723
problem = numcloud([1,2,3,4], [(1,2),(1,3),(1,4),(2,1),(3,4)])

# ╔═╡ 2a758fdf-2a8c-4e27-9069-82251e3c25be
#testing the forward checking
forward_check(problem, x1)

# ╔═╡ 8eebad3f-c17e-4f87-ba14-8a38880cd722
solve_cloud(problem,[])

# ╔═╡ d0a9798f-512b-4605-a9a2-18947eb8f7da


# ╔═╡ eb621b49-a26b-40c2-98d7-2951b634fd0b
using Pkg

# ╔═╡ 5d0b9c2c-aae0-4d0d-b079-8c7b4d33791f
begin
 import Pkg
end

# ╔═╡ Cell order:
# ╠═c046cf40-cbe8-11ec-23f8-37cc023be132
# ╠═3bbc4486-6ef1-47c2-a9f5-df976ef6a11b
# ╠═b0f78aa0-137f-4f73-bb0a-7d6a9e7f0dd9
# ╠═914c1846-0ca3-4f26-9d34-6e836d091630
# ╠═8c1cee7e-4fff-4484-9c9e-708fc94bfc4a
# ╠═5d0b9c2c-aae0-4d0d-b079-8c7b4d33791f
# ╠═2e8211c4-b663-49b4-bf58-a80ab3777698
# ╠═83221e07-9284-4034-bd13-1b5fb2fce504
# ╠═eb621b49-a26b-40c2-98d7-2951b634fd0b
# ╠═19286654-d654-44d5-8b7f-73507b45889a
# ╠═24083e91-f694-4c65-b71f-573d8c1ee002
# ╠═cefa56d1-d2e4-4269-96a2-b98975319d5e
# ╠═a2f19e69-fd5b-42cf-9a2d-e2dc48979bed
# ╠═98b01c86-1ca6-4932-973b-8c1cd2772f0a
# ╠═739a2c22-2de3-4ac0-8ff9-35a615b8816f
# ╠═22df6018-4ac6-443a-bc36-49fb68901b19
# ╠═ff8bd2d0-dafa-437b-aedf-651af3df3f50
# ╠═77b77df9-dd60-450a-b4fa-f06c1b2f5a69
# ╠═8695804e-3fbb-4c6a-9a77-d0f80016985d
# ╠═1d414422-9b46-400d-be57-dc7fb5b81b51
# ╠═5f15513f-fcc3-43f5-beb4-d308ada577e2
# ╠═7ccfd548-d31d-4432-a480-e3b93bfed7ea
# ╠═4eeba374-7634-48dc-96a9-70fb2dc030bc
# ╠═0065f708-8326-47e9-baf1-66ecfc7d7dda
# ╠═6992ebb4-5611-4944-8d77-e970a2455aa3
# ╠═fb88765d-c402-4fab-92cd-ba793a97a723
# ╠═2a758fdf-2a8c-4e27-9069-82251e3c25be
# ╠═8eebad3f-c17e-4f87-ba14-8a38880cd722
# ╠═d0a9798f-512b-4605-a9a2-18947eb8f7da
